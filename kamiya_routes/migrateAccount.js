require('dotenv').config();

const express = require('express');
const bodyparser = require('body-parser');
const request = require('then-request');

const { Configuration, OpenAIApi } = require('openai');
const kv = require("../kamiya_modules/key-value");
const fs = require("fs");
const imageUpload = require("../kamiya_modules/image_upload");
const cache = require('../kamiya_modules/session_cache');

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

process.env.TZ = 'Asia/Shanghai';

const app = express.Router();

app.use(bodyparser.json({ limit:'1000mb'}));

const D = new kv('./data/data.json');

const O = new kv('./data/openid.json');

app.post('/api/migrateAccount/getDetails', async (req, res) => {
    const { token, adminPass } = req.body;
    if(adminPass === process.env.ADMINPASS) {
        let left = D.get(`${token}.left`);
        left = Math.floor(left) * 100;
        if(D.get(`${token}.available`)) {
            const ucL = JSON.parse(fs.readFileSync('./data/user_content.json').toString());
            const userContent = ucL[token] || [];
            let bind = O.get(`${token}.from`) || 'Console Console';
            bind = bind.split(' ');
            res.json({
                success: true,
                credit: left,
                userContent: userContent,
                bind: {
                    platform: bind[0],
                    bindId: bind[bind.length - 1]
                }
            });
        } else {
            res.json({
                success: false,
                message: 'Token not found'
            });
        }
    } else {
        res.json({
            success: false,
            message: 'Admin password incorrect'
        });
    }
});

app.post('/api/migrateAccount/finishMigrate',(req, res) => {
    const { token, adminPass } = req.body;
    if(adminPass === process.env.ADMINPASS) {
        D.put(`${token}.available`, false);
        res.json({
            success: true
        });
    } else {
        res.json({
            success: false,
            message: 'Admin password incorrect'
        });
    }
});

module.exports = app;