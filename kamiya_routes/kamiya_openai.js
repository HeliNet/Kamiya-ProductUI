require('dotenv').config();

const express = require('express');
const bodyparser = require('body-parser');
const request = require('then-request');

const { Configuration, OpenAIApi } = require('openai');
const kv = require("../kamiya_modules/key-value");
const fs = require("fs");
const imageUpload = require("../kamiya_modules/image_upload");
const cache = require('../kamiya_modules/session_cache');

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

process.env.TZ = 'Asia/Shanghai';

const app = express.Router();

app.use(bodyparser.json({ limit:'1000mb'}));

const D = new kv('./data/data.json');

const S = new kv('./data/pass.json');

const C = new kv('./data/openai_conversation.json');

const P = new kv('./data/openai_profile.json');

const tokenizer = require('gpt-3-encoder')

function sizeContent(t) {
    return tokenizer.encode(t).length;
}

function check_pass(pass) {
    if(cache.getCheck(pass)) return true;
    else {
        if(!S.get(`${pass}.logout`) && S.get(`${pass}.token`) && (Date.parse(new Date()) - S.get(`${pass}.time`)) < 604800000) {
            cache.setCheck(pass);
            return true;
        }
        return false;
    }
};

function uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
}

function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),
        "m+": (date.getMonth() + 1).toString(),
        "d+": date.getDate().toString(),
        "H+": date.getHours().toString(),
        "M+": date.getMinutes().toString(),
        "S+": date.getSeconds().toString()
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
        };
    };
    return fmt;
}

function get_token(pass) {
    if(cache.getSession(pass)) {
        return cache.getSession(pass);
    }
    else {
        const token = S.get(`${pass}.token`);
        cache.setSession(pass,token);
        return token;
    }
};
function get_left(token) {return D.get(`${token}.left`);};

function processCtx(c) {
    total = 0;
    for(let i in c) {
        total += sizeContent(c[i].content);
    }
    if(total < 4095) return c;
    else {
        let i = 4;
        while(total > 4095) {
            total -= sizeContent(c[i].content);
            c.splice(i,1);
            i++;
        }
        return c;
    }
}

app.post('/api/openai/conversation',async (req,res) => {
    //console.log(req.body);

    const pass = req.body.pass;

    if(!pass || !check_pass(pass)) {res.send({success: false});return;};
    const token = get_token(pass);
    if(Number(get_left(token)) < 0) {res.send({success: false,message: '剩余魔晶点数不足'});return;};

    const action = req.body.action;
    let conversation_id = req.body.conversation_id;

    const defaultRole = [
        {'role': 'system', 'content': '你是由OpenAI开发的人工智能助手，现在的时间是' + dateFormat("YYYY-mm-dd HH:MM:SS",new Date())},
        {'role': 'user', 'content': '你是谁？'},
        {'role': 'assistant', 'content': '你好，我是由OpenAI创造的人工智能。'}
    ];

    let ctx;

    if(!conversation_id && req.body.head) {
        try {
            ctx = JSON.parse(req.body.head);
        }
        catch (e) {
            ctx = defaultRole;
        }
        conversation_id = uuid();
    }

    if(!conversation_id && req.body.role) {
        ctx = defaultRole;
        const list = JSON.parse(fs.readFileSync('./data/role.json'));
        if(list[req.body.role]) {
            ctx.push({'role': 'user', 'content': list[req.body.role].user});
            ctx.push({'role': 'assistant', 'content': list[req.body.role].assistant});
        }
        conversation_id = uuid();
    }

    if(conversation_id && !ctx) {
        ctx = C.get(conversation_id);
        try {
            ctx = JSON.parse(ctx);
        }
        catch(e) {
            ctx = defaultRole;
        }
    }
    else if(!ctx && !conversation_id) {
        conversation_id = uuid();
        ctx = defaultRole;
    }

    ctx.push({'role': 'user', 'content': req.body.prompt});

    let o = JSON.parse(fs.readFileSync('./logs/chatdemo.json'));
    if(o[dateFormat("YYYY-mm-dd",new Date())]) o[dateFormat("YYYY-mm-dd",new Date())] += 1;
    else o[dateFormat("YYYY-mm-dd",new Date())] = 1;
    fs.writeFileSync('./logs/chatdemo.json',JSON.stringify(o,0,2));

    ctx = processCtx(ctx);

    openai.createChatCompletion({
        model: 'gpt-3.5-turbo',
        messages: ctx,
        stream: true
    },{
        responseType: 'stream'
    }).then((R) => {
        //console.log(R);
        res.set({
            'Cache-Control': 'no-cache',
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive'
        });
        res.flushHeaders();
        let result = '',lock = false;
        R.data.on('data', (chunk) => {
            //console.log(chunk.toString());
            if(chunk.toString().match(/data: \[DONE\]/)) {
                res.write(chunk.toString().replace('\[DONE\]',JSON.stringify({id: 'kamiya_info', conversation_id: conversation_id, message: '[DONE]', full_content: result})));
                D.put(`${token}.left`,(get_left(token) - 1).toFixed(1) * 1);
                ctx.push({'role': 'assistant', 'content': result});
                C.put(conversation_id,JSON.stringify(ctx));
                return;
            }
            if(chunk.toString().match(/data: \[DONE\]/) && lock) {
                res.write(chunk.toString().replace('\[DONE\]',JSON.stringify({id: 'kamiya_lock', conversation_id: conversation_id, message: '[DONE]', full_content: result})));
                D.put(`${token}.left`,(get_left(token) - 1).toFixed(1) * 1);
                ctx.push({'role': 'assistant', 'content': result});
                C.put(conversation_id,JSON.stringify(ctx));
                return;
            }
            if(lock) return;
            try{
                const J = JSON.parse(chunk.toString().replace('data: ',''));
                if(J.choices[0].delta) {
                    if(J.choices[0].delta.content) result += J.choices[0].delta.content;
                    if(result.match((/\[BLOCKED\]/))) {
                        C.put(conversation_id,JSON.stringify([]));
                        lock = true;
                        return;
                    }
                }
            }
            catch (e) {
                console.log(e);
            }
            res.write(chunk.toString());
        });
        R.data.on('end', () => {
            res.end();
        });
        req.on('close', () => {});
        /*
        if(result.content.match((/\[BLOCKED\]/))) {
            res.send({
                success: false,
                conversation_id: conversation_id,
                message: '会话异常，已中断此会话' + '，将此ID上报以快速定位此次错误' + conversation_id
            });
            console.log({
                success: false,
                conversation_body: req.body,
                result: result
            });
            C.put(conversation_id,JSON.stringify([]));
            return;
        }
        res.send({
            success: true,
            conversation_id: conversation_id,
            result: result.content
        });
        console.log({
            success: true,
            conversation_body: req.body,
            result: result
        });
         */
    },(e) => {
        console.log(e.response.data.error);
        res.send({
            success: false,
            conversation_id: conversation_id,
            message: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversation_id
        });
        console.log({
            success: false,
            conversation_body: req.body,
            message: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversation_id
        });
    });
});

app.get('/api/openai/get_context',(req,res) => {
    if(req.query.id){
        res.setHeader('Content-Type','application/octet-stream');
        res.setHeader('Content-Disposition','attachment; filename=kamiya_chatgpt_demo_context_' + req.query.id + '_' + dateFormat('YYYY-mm-dd_HH-MM-SS',new Date()) + '.json');
        let ctx = C.get(req.query.id);
        if(ctx) {
            try {
                ctx = JSON.parse(ctx);
                delete ctx[2];
                delete ctx[3];
            }
            catch (e) {
                ctx = [];
            }
        }
        res.send({
            success: true,
            context: ctx
        });
    }
    else res.send({success: false});
});

app.post('/api/generate-voice',(req,res) => {
    const pass = req.body.pass;

    const u = uuid();
    console.log({
        req: req.body,
        uuid: u
    })

    if(!pass || !check_pass(pass)) {res.send({success: false});return;};
    const token = get_token(pass);
    if(!(get_left(token) > 0)) {res.send({success: false,message: '剩余魔晶点数不足'});return;};

    let o = JSON.parse(fs.readFileSync('./logs/tts.json'));
    if(o[dateFormat("YYYY-mm-dd",new Date())]) o[dateFormat("YYYY-mm-dd",new Date())] += 1;
    else o[dateFormat("YYYY-mm-dd",new Date())] = 1;
    fs.writeFileSync('./logs/tts.json',JSON.stringify(o,0,2));

    let cost = 0.5;

    if(req.body.prompt.length > 300) {
        res.send({
            success: false,
            message: '请求的文字过长，将此ID上报以快速定位此次错误' + u
        });
        return;
    }

    const backendL = JSON.parse(fs.readFileSync('./data/backend.json'));

    switch(req.body.type) {
        case 'moegoe': {
            const node = backendL.moe[Math.floor(Math.random() * backendL.moe.length)];
            request('POST',node.url,{json: {
                    model_name: node.model,
                    task_id: Math.floor(Math.random() * 1024),
                    text: '[ZH]' + req.body.prompt + '[ZH]',
                    speaker_id: 0
                }}).getBody('utf-8').then(JSON.parse).done((R) => {
                    if(R.code == 200) {
                        res.send({
                            success: true,
                            audio: R.audio,
                            type: 'moegoe',
                            speaker: R.speaker,
                            speaker_pic: node.pic_url
                        });
                        D.put(`${token}.left`,(get_left(token) - cost).toFixed(1) * 1);
                    }
                    else {
                        res.send({
                            success: false,
                            message: R.msg + '，音频合成后端错误，将此ID上报以快速定位此次错误' + u
                        });
                    }
            },(e) => {
                    console.log(e);
                    res.send({
                        success: false,
                        message: '音频合成后端错误，将此ID上报以快速定位此次错误' + u
                    });
            });
            break;
        }
        case 'azure': {
            break;
        }
        default: {
            break;
        }
    }

});

app.get('/api/get-profile',(req,res) => {
    const pass = req.query.pass;
    if(!pass || !check_pass(pass)) {res.send({success: false});return;};
    const token = get_token(pass);
    const profile = P.get(`${token}.profile`) || 'default|default';
    profile = profile.split('|');
    res.send({
        success: true,
        pic: profile[0],
        name: profile[1]
    });
});

app.post('/api/set-profile',(req,res) => {
    const pass = req.body.pass;
    const pic = req.body.pic;
    const name = req.body.name;
    if(!pass || !check_pass(pass)) {res.send({success: false});return;};
    const token = get_token(pass);
    if(pic.length > 4000000) {res.send({success: false,message: '图片过大'});return;};
    imageUpload(pic).then((R) => {
        P.put(`${token}.profile`,`${R}|${name}`);
        res.send({
            success: true
        });
    });
});

module.exports = app;